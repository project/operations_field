# Operations field

Adds a new base field to all content entities for displaying the list of entity operations.
 
This field can render the operations links in a display mode or in a view.
